#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <time.h>

#define N 3

#define IDLE 0
#define WAITING 1
#define WORKING 2

#define ELETRO 0
#define PRESSAO 1
#define ESPIRO 2

#define LEFT (x+N-1)%N
#define RIGHT (x+1)%N

const char FILENAME[] = "output.txt";

void forkResult(void);
void test(int);
void read_buf(void);
void show(void);
void handleSignal(int sig, siginfo_t *info, void *ptr);
void finish(int);

int state[N];
pid_t fork_result[N];

int data_processed;
int file_pipes[2];
char buffer[BUFSIZ + 1];

FILE *f;

/**
 * MAIN FUNCTION - Set the Signal Handler, Create forks, then run forever
 */
int main() {

    // [BGN] ----- SIGNAL CONFIG -----
    (void) signal(SIGINT, finish); //Set handle for CTRL^C command
    struct sigaction act;
    act.sa_sigaction = handleSignal;
    act.sa_flags = SA_SIGINFO;
    sigaction(SIGALRM, &act, 0);
    // [END] ----- SIGNAL CONFIG -----

    // [BGN] ----- OPEN FILE -----
    f = fopen(FILENAME, "w");
    if (f == NULL)
    {
        printf("Error opening file!\n");
        exit(1);
    }
    // [END] ----- OPEN FILE -----

    // [BGN] ----- STATES CONFIG -----
    int i;
    for(i=0; i<N; i++) {
        state[i]= IDLE;
    }
    show();
    // [END] ----- STATES CONFIG -----

    // [BGN] ----- CREATE FORKS -----
    memset(buffer, '\0', sizeof(buffer));
    if (pipe(file_pipes) == 0) {
        // [BGN] ----- ELETRO FORK -----
        fork_result[ELETRO] = fork();
        if(fork_result[ELETRO] == -1)  forkResult();
        if(fork_result[ELETRO] == 0) {
            sprintf(buffer, "%d", file_pipes[1]); //Passes to the child the write pipe
            (void)execl("eletro", "eletro", buffer, NULL);
            exit(EXIT_FAILURE);
        }
        // [END] ----- ELETRO FORK -----

        // [BGN] ----- PRESSAO FORK -----
        fork_result[PRESSAO] = fork();
        if(fork_result[PRESSAO] == -1)  forkResult();
        if(fork_result[PRESSAO] == 0){
            sprintf(buffer, "%d", file_pipes[1]); //Passes to the child the write pipe
            (void)execl("pressao", "pressao", buffer, NULL);
            exit(EXIT_FAILURE);
        }
        // [END] ----- PRESSAO FORK -----

        // [BGN] ----- ESPIRO FORK -----
        fork_result[ESPIRO] = fork();
        if(fork_result[ESPIRO] == -1)   forkResult();
        if(fork_result[ESPIRO] == 0){
            sprintf(buffer, "%d", file_pipes[1]); //Passes to the child the write pipe
            (void)execl("espiro", "espiro", buffer, NULL);
            exit(EXIT_FAILURE);
        }
        // [END] ----- ESPIRO FORK -----
    // [END] ----- CREATE FORKS -----


        while(1); //RUN PROGRAM FOREVER
        exit(EXIT_SUCCESS);
    }
}

/**
 * Shows the state of child processes
 */
void show() {
    int i;
    for(i=0; i<N; i++) {
        char *name;
        if( i == 0 ) name = "Eletro";
        if( i == 1 ) name = "Pressao";
        if( i == 2 ) name = "Espiro";

        if(state[i] == IDLE) {
            printf("%s esta coletando dados!\n", name);
        }
        if(state[i] == WAITING) {
            printf("%s esta aguardando!\n", name);
        }
        if(state[i] == WORKING) {
            printf("%s esta transmitindo!\n", name);
        }
    }
    printf("\n");
}

/**
 * Receive the signal sent by the child process.
 * If the current state of the process is IDLE then it is requesting writing in the pipe
 *   - In this case change the state and test if it can write
 * If the current state of the process is WORKING then it is releasing the pipe
 *   - In this case change the state and test if the child in the left can write, then the right child
 */
void handleSignal(int sig, siginfo_t *info, void *ptr) {
    int pid = info->si_pid;
    int x;
    for(x = 0; x < N; x++) {
        if(fork_result[x] == pid) {
            if(state[x] == IDLE) { //The child process informed the need to write on the pipe
                state[x] = WAITING;
                show();
                test(x);
                break;
            }

            if(state[x] == WORKING) { //The child process reported that it have already used the pipe
                state[x] = IDLE;
                show();
                test(LEFT);
                test(RIGHT);
                break;
            }
        }
    }
}

/**
 * Handles the CTRL^C command and close the stream to the output file, then exit the program
 */
void finish(int sig){
    fclose(f); //Close output file
    exit(1); //Exit program
}

/**
 * Then tests if the child, in the given position, can write in the pipe
 * If the child is allowed to write, then sends back the signal, change the state and read the pipe
 * @param {int} pid A child's process ID
 */
void test(int x) {
    if(state[x] == WAITING && state[LEFT] != WORKING && state[RIGHT] != WORKING) {
        kill(fork_result[x], SIGALRM);
        state[x] = WORKING;
        show();
        read_buf();
    }
}

/**
 * Read the buffer then saves its content in a file
 */
void read_buf() {
    memset(buffer, '\0', sizeof(buffer));
    data_processed = read(file_pipes[0], buffer, BUFSIZ);

    // ---------
    char time_string[1000];
    time_t t = time(NULL);
    struct tm * p = localtime(&t);
    strftime(time_string, 1000, "%D %H:%M:%S", p);
    // -----

    fprintf(f, "%s - %s\n", time_string, buffer);
}

/**
 * Informes the failure then exit program
 */
void forkResult() {
    printf("Falha ao Criar Fork. Encerrando...\n");
    fprintf(stderr, "Fork Failure");
    exit(EXIT_FAILURE);
}
