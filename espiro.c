#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <time.h>

void collect_data(void);
void request_writing(void);
void releae_writing(void);
void write_pipe(void);
void handleSignal(int);

int data_processed;
char buffer[BUFSIZ + 1];
int file_deor;



/**
 * MAIN FUNCTION - Set the signal handler. Then in loop: collect data, request write permission, wait for permission
 * sends data, release write
 */
int main(int argc, char *argv[]) {

    srandom(time(NULL)); //Seed for random()

    sscanf(argv[1], "%d", &file_deor); //Saves the write pipeline file_descriptor


    (void) signal(SIGALRM, handleSignal); //Set the Signal Handler
    union sigval value;

    while(1) { //Run forever
        collect_data();

        sigqueue(getppid(), SIGALRM, value); //Sends signal to father process requesting write permission
        pause(); //Wait for father signal back - That means 'ok, you can write in the pipe'

        write_pipe();

        sigqueue(getppid(), SIGALRM, value); //Sends signal to father process releasing write permission
    }

    exit(EXIT_SUCCESS);
}

/**
 * Handle Received Signal. Do nothing,
 */
void handleSignal(int sig) {
    return ;
}

/**
 * Since the program dont read any real sensor. Just wait a time
 */

void collect_data() {
    // --- Wait a time
    int r = (int)(0.001*random());
    usleep(r);
    // --- END
}

/**
 * Generates a random real number, then Assemble the shipping string and sends it.
 * Wait a time to fake a transaction time
 */
void write_pipe() {

    // --- Generates a random real number and cast to char
    int randomNumb = random() % (5 + 1 - 3) + 3;
    char str[2];
    sprintf(str, "%d", randomNumb);
    // --- END

    // --- Concatenates all data eg: 'ESPIRO: 5VEF1'
    char data[15] = "ESPIRO: ";
    strcat(data, str);
    strcat(data, "VEF1");
    // --- END

    data_processed = write(file_deor, data, strlen(data)); //Sends it through the pipe

    // --- Wait a time
    int r = (int)(0.001*random());
    usleep(r);
    // --- END
}
